# README #

This README documents the OGG ontology and the OGG repository information. 

### What is this ogg repository for? ###

* This Bitbucket repository is for OGG: the Ontology of Genes and Genomes. 

### What is OGG? ###

* OGG is a biological ontology in the area of genes and genomes. OGG uses the Basic Formal Ontology (BFO) as its upper level ontology. This OGG document contains the genes and genomes of a list of selected organisms, including human, two viruses (HIV and influenza virus), and bacteria (B. melitensis strain 16M, E. coli strain K-12 substrain MG1655, M. tuberculosis strain H37Rv, and P. aeruginosa strain PAO1). More OGG information for other organisms (e.g., mouse, zebrafish, fruit fly, yeast, etc.) may be found in other OGG subsets.

### OGG discussion ###

* Use the Bitbucket issue tracker: https://bitbucket.org/hegroup/ogg/issues/ 
* Google OGG Discussion group email: ogg-discuss@googlegroups.com
* Google OGG Discussion group: https://groups.google.com/forum/#!forum/ogg-discuss
* Contact Oliver He, University of Michigan Medical School, Ann Arbor, MI 48105, USA: http://www.hegroup.org

### OGG in OBO ###

* [OGG in OBO library ontology website](http://obofoundry.org/ontology/ogg.html)
* [OBO Foundry](http://obofoundry.org/)

### OGG browsing ###

* [OGG in Ontobee](http://www.ontobee.org/ontology/ogg)
* [OGG in BioPortal](https://bioportal.bioontology.org/ontologies/OGG)


### Other OGG subsets ###

* Additional OGG subsets are for different organisms:
* OGG-C. elegans: http://www.ontobee.org/ontology/OGG-Ce
* OGG-Fruit Fly: http://www.ontobee.org/ontology/OGG-Dm
* OGG-Mouse: http://www.ontobee.org/ontology/OGG-Mm
* OGG-P. falciparum: http://www.ontobee.org/ontology/OGG-Pf
* OGG-Yeast: http://www.ontobee.org/ontology/OGG-Sc
* OGG-Zebrafish: http://www.ontobee.org/ontology/OGG-Dr

### OGG publication ###
* He Y, Liu Y, Zhao B. OGG: a biological ontology for representing genes and genomes in specific organisms. Proceedings of the 5th International Conference on Biomedical Ontologies (ICBO), Houston, Texas, USA. October 8-9, 2014. Pages 13-20. URL: http://ceur-ws.org/Vol-1327/icbo2014_paper_23.pdf or: http://www.hegroup.org/docs/OGG-ICBO2014.pdf (with correctly formatted Figure 3).